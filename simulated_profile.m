%one site is 10bp
clear;
d=500;%diffusion constant site^2/s
dB=10*log(2)/60;%unloading rate, hydrolysis rate is sqrt()log(2)/70
kB=200*dB;%loading rate, divided by 30-170 depending on the parS site
kR=0*0.01/75;%0.01/75, initiation per sec per promoter. multiplied by 31-113 depending on promoter
ke=40/10;%translocation rate of RNAP,  75 nt/s
Btot=300;
%bound B is
Btot*kB*(1/30+1/62+1/133+1/144+1/170)/(dB+kB*(1/30+1/62+1/133+1/144+1/170))

cmd=['set path=%path:C:\Program Files\MATLAB\R2018b\bin\win64;=% & bin\Release\ParB_sliding.exe ',num2str(d),' ', num2str(dB),' ',num2str(kB),' ',num2str(kR),' ', num2str(ke),' ',num2str(Btot)];
tic
[status, cmdout]=system(cmd,'-echo');%outputs to current matlab directory
toc
%%
file=fopen('ParB.txt');
tline=fgetl(file);
sParB=[];
total=[];
while ischar(tline)
    z=textscan(tline,'%f','Delimiter','\t');%outputs cell array
    sParB=[sParB; z{1}];
    total=[total, length(z{1})];
    tline=fgetl(file);
end
fclose(file);
%%
%format long;

figure(1)
clf;
range=4030000:4042929;
start=4030550;%first box in simulation
edge1=range(1)-25;
edge2=range(end)-25;
histogram(10*sParB+4030550,[edge1:50:edge2])
xlim([edge1,edge2])
xticks(1000*[4030  4032 4034 4036 4038 4040 4042])
xticklabels({'4,030'  '4,032' '4,034' '4,036' '4,038' '4,040' '4,042'})
% xtickformat('%,.3f');
% ax=get(gca,'axes');
% ax.XAxis.Exponent = 3;
pos = get(gca, 'Position');
c=@(x) pos(1)+(x-edge1)/(edge2-edge1)*pos(3);

annotation('arrow',c([4034791 4030550]),[pos(2) pos(2)]-0.1*pos(4));
annotation('arrow',c([4036564 4034963]),[pos(2) pos(2)]-0.1*pos(4));
annotation('arrow',c([4036637 4038351]),[pos(2) pos(2)]-0.1*pos(4));
annotation('arrow',c([4039866 4038434]),[pos(2) pos(2)]-0.1*pos(4));
annotation('arrow',c([4042684 4040126]),[pos(2) pos(2)]-0.1*pos(4));
xlabel('Position (kb)');
hold on;
h=plot([4034957, 4034896,4037790,4033217,4035415],[0,0,0,0,0],'or','MarkerSize',5,'MarkerFaceColor','r');

z=load('ChIP_data.mat');

plot(range,z.ParB(range)/40,'Linewidth',2,'DisplayName','ParB ChIP-Seq');
legend;
hold off


