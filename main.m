
%C=getgenbank('NC_011916');%,'FileFormat','GenBank','tofile','NA1000.txt');
load('Chip_data');

%C=genbankread('sequence.gb');
%save('sequence.mat','C');
load('sequence.mat')

%%
RpoC_n=RpoC/sum(RpoC);
control_n=control/sum(control);
RpoC_n(RpoC_n==0)=NaN;
control_n(control_n==0)=NaN;

figure(1)
plot(1:4042929,RpoC_n,1:4042929,control_n)
%plot(1:4042929,log2(Rpoc_n./control_n))
%%

for i=1:length(C.CDS)
    C.CDS(i).RpoC=sum(RpoC(C.CDS(i).indices))/length(C.CDS(i).indices);
end
%%
%operons
op=[4030550,4034791;
    4034963,4036564;
    4036637, 4038351;
    4038434, 4039866;
    4040126, 4042684];

for i=1:size(op,1)
    mean(4.7*RpoC(op(i,1):op(i,2))-control(op(i,1):op(i,2)))
end



%%

range=4030000:4042929;
%range=[4032935:4036691];
figure(2)
clf;
%plot(range,4*RpoC_n(range),range,control_n(range))
plot(range,4.7*RpoC(range),range,control(range))
%plot(range,log2(Rpoc_n(range)./control_n(range)))
line([4032305,4031502],[0,0],'LineWidth',2)%parA
line([4032945,4032295],[10,10],'LineWidth',2)%gidB
line([4034791,4032935],[0,0],'LineWidth',2)%gidA
line([4036332,4034962],[10,10],'LineWidth',2)%trmE
line([4039866,4038433],[0,0],'LineWidth',2)%rho
line([4040640,4040125],[10,10],'LineWidth',2)%CCNA_03877
xlim([range(1),range(end)])

pos = get(gca, 'Position');
c=@(x) pos(1)+(x-range(1))/(range(end)-range(1))*pos(3);
annotation('arrow',c([4034791 4030550]),[pos(2) pos(2)]-0.1*pos(4));
annotation('arrow',c([4036564 4034963]),[pos(2) pos(2)]-0.1*pos(4));
annotation('arrow',c([4036637 4038351]),[pos(2) pos(2)]-0.1*pos(4));
annotation('arrow',c([4039866 4038434]),[pos(2) pos(2)]-0.1*pos(4));
annotation('arrow',c([4042684 4040126]),[pos(2) pos(2)]-0.1*pos(4));

hold on;
plot([4034957, 4034896,4037790,4033217,4035415],[0,0,0,0,0],'x','MarkerSize',10);
hold off;
%%
j=1;
for a=1:0.1:7
y=a*RpoC_n-control_n;
p(j)=nansum(y>=0)/sum(~isnan(y));
j=j+1;
end
figure(4)
plot(1:0.1:7,p)
%%





