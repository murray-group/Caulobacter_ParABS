#ifndef definitions_H_INCLUDED
#define definitions_H_INCLUDED
# include <vector>		// std::vector<>
# include <array>
# include <memory>

#include <algorithm>
#include <functional>

//# define NDEBUG //comment out to turn on assert. costs ~10%
# include <cassert>	// for assert()

//_______________________________________________



class Reaction; //forward declaration
typedef class std::shared_ptr<Reaction>	ReactionPtr ; // a handy nickname
typedef std::vector<ReactionPtr>	ReactionsArray ; // a handy nickname, vector cannot contain Reaction, only Reaction* since Reaction is an abstract class



template <typename T>
std::vector<T> concat(const std::vector<T> &A, const std::vector<T> &B)//concatenate vectors, returns new vector
{
    std::vector<T> AB;
    AB.reserve( A.size() + B.size() );                // preallocate memory
    AB.insert( AB.end(), A.begin(), A.end() );        // add A;
    AB.insert( AB.end(), B.begin(), B.end() );        // add B;
    return AB;
}

template <typename T>
std::vector<T> & concat_in_place(std::vector<T> &A, const std::vector<T> &B)//concatenate vectors, appends the second to the first
{
    //A.reserve( A.size() + B.size() );                // preallocate memory
    A.insert( A.end(), B.begin(), B.end() );        // add B;
    return A;
}




template <typename T>
std::vector<T> operator+(const std::vector<T> &A, const std::vector<T> &B)//add vectors, returns new vector
{
    assert(A.size()==B.size());

    std::vector<T> result;
    result.reserve( A.size());                // preallocate memory

    std::transform(A.begin(), A.end(), B.begin(), std::back_inserter(result), std::plus<T>());
    return result;
}

template <typename T>
std::vector<T> & operator+=(std::vector<T> &A, const std::vector<T> &B)//+= add vectors, returns by reference
{
    assert(A.size()==B.size());

    std::transform(A.begin(), A.end(), B.begin(), A.begin(), std::plus<T>());
    return A;
}


template <typename T>
std::vector<T> operator+(const std::vector<T> &A, T &B)//add scalar, returns new vector
{
    std::vector<T> AB;
    AB.reserve( A.size());                // preallocate memory
    AB=A;
    for(T& d : AB)
    {
        d += B;
    }
    return AB;
}

template <typename T>
std::vector<T> & operator+=(std::vector<T> &A, T &B)//+= add scalar, returns by reference
{
    for(T& d : A)
    {
        d += B;
    }
    return A;
}



//enum { A = 0, B, C, D, E} ;

// See http://physics.nist.gov/cgi-bin/cuu/Value?na
const double	Avogadro_const = 6.02214179e23 ;



//_____





#endif // definitions_H_INCLUDED
