#ifndef SPECIES_H
#define SPECIES_H
# include <fstream>
# include <iostream>	// cout, etc.
#include <sstream>      // std::stringstream
//#include "model_def.h"
const size_t start=403055;
const size_t L=404268-start+1;//length of centromeric region



class Species
{
public:

    std::array<bool,L> R;//RpoC occupancy
    std::array<bool,L> B;//ParB
    std::vector<size_t> Bpos;//unordered list of positions


    std::array<std::vector<size_t>,5> Rpos;//one for each operon


    Species():R{0},B{0},Bpos{},Rpos{}{
        Bpos.reserve(L);
        Rpos[0].reserve(403479-start+1);
        Rpos[1].reserve(403656-403496+1);
        Rpos[2].reserve(403835-403664+1);
        Rpos[3].reserve(403987-403843+1);
        Rpos[4].reserve(404268-404013+1);
    };

    virtual ~Species() {};


    void print(std::ofstream &out){
        std::stringstream buffer;

     for(auto pos : Bpos){
        buffer << pos<< '\t';
     }
        buffer << '\n';
    out << buffer.str();

    }


    bool in_region(long int i){
    return i>=long(0) && i<long(L);

    }

protected:
private:

};





#endif // SPECIES_H
