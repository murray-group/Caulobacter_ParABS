#ifndef UNBINDINGB_H
#define UNBINDINGB_H

#include <Reaction.h>


class unbindingB : public Reaction
{
    public:
        unbindingB(double k) {
                rate_const=k;}

        virtual ~unbindingB() {}


        void do_reaction(Species &P, double rx_u, double rate){
            size_t i=rx_u/rate_const;//index in Bpos

                P.B[P.Bpos[i]]=0;
                P.Bpos.erase(P.Bpos.begin()+i);
                reactionsAffected={0,1,2,3,4,5,6};//affects diffusionB, bindingB and unbindingB
                //std::cout << "unbinding" << '\n';
                return;

        }


        double rate(const Species &P){
            return rate_const*P.Bpos.size();
        }




    protected:

    private:
};

#endif // UNBINDINGB_H
