#ifndef TwoDSEARCH_H
#define TwoDSEARCH_H
# include <vector>
# include <cmath> //floor() etc
# include <cassert>
#include <tuple>
#include <iostream>//cout
#include <limits> //machine precision
# include <numeric>
# include <math.h> //signbit


class TwoDsearch
{
public:

    TwoDsearch();
    virtual ~TwoDsearch();

    TwoDsearch(size_t n_react);
    //TwoDsearch(const std::vector<std::vector<size_t>> reactionlists); // TO DO : implement ordering and the necessary storage of reaction num <-> matrix position associations

    std::tuple<size_t,double, double> search_obj_search(const double rx_u) const;
    void update_search_obj(size_t rx,double rate);
    double get_root_value() const;

protected:

private:
    double double_precision=std::numeric_limits<double>::epsilon();
    size_t L;//matrix size
    std::vector<std::vector<double>> M;//matrix of propensities
    std::vector<double> rowsums;//matrix of propensities
    double totalsum;

    std::vector<double> errorupperboundrows;
    double errorupperboundtotalsum;

    //std::vector<std::pair<size_t,size_t>> matrix_indices;//indices of matrix entry for each reaction
    //std::vector<std::vector<size_t>> reaction_nums;//reaction whos rate is stored at each matrix position







};

#endif // 2DSEARCH_H
