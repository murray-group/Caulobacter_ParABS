#ifndef ELONGATION_H
#define ELONGATION_H

#include <Reaction.h>


class elongation : public Reaction
{
public:
    const int option=2;
    size_t left;
    size_t right;
    int dir;
    size_t op;


    elongation(double k,size_t left,size_t right,int dir,size_t op):left(left),right(right),dir(dir),op(op)
    {
        rate_const=k;
    }

    virtual ~elongation() {}



    void do_reaction(Species &P, double rx_u, double rate)
    {
        if(P.Rpos[op].size()==0) return;

        size_t i=rx_u/rate_const;//index in Rpos[op]
        long int newpos=P.Rpos[op][i]+dir;
        //std::cout << dir << '\n';
        assert(i<P.Rpos[op].size());

        if(newpos==(long(left)-1)*(1-dir)/2+(long(right)+1)*(dir+1)/2) //fall off the edge
        {
            P.R[P.Rpos[op][i]]=0;
            P.Rpos[op].erase(P.Rpos[op].begin()+i);
            reactionsAffected= {op+7}; //affects itself
            return;
        }


        switch(option)
        {
        case 0:  //RNAP only moves if next site is free
        {
            if(P.R[newpos]==0 && P.B[newpos]==0)
            {
                P.R[P.Rpos[op][i]]=0;
                P.R[newpos]=1;
                P.Rpos[op][i]=newpos;
            }
            break;
        }

        case 1: //RNAP moves if there is no other RNAP at the next site, pushes any ParB along
        {
            if(P.R[newpos]==0)
            {
                P.R[P.Rpos[op][i]]=0;
                P.R[newpos]=1;
                P.Rpos[op][i]=newpos;


                if(P.B[newpos]==1)
                {
                    long int j=newpos;
                    while(P.in_region(j) && P.B[j]==1)
                    {
                        j=j+dir;
                    }

                    if(P.in_region(j))
                    {
                        P.B[j]=1;
                        P.B[newpos]=0;

                        //find index in Bpos vector
                        auto it = std::find(P.Bpos.begin(), P.Bpos.end(), newpos);
                        *it=size_t(j);
                        reactionsAffected= {};
                    }
                    else
                    {
                        P.B[newpos]=0;
                        auto it = std::find(P.Bpos.begin(), P.Bpos.end(), newpos);

                        P.Bpos.erase(it);
                        reactionsAffected= {0,1,2,3,4,5,6};//B is removed so diffusionB, bindingB unbindingB need to be updated
                    }
                }
            }
            break;
        }

        case 2: //RNAP moves if there is no other RNAP at the next site, kicks ParB off
        {
            if(P.R[newpos]==0)
            {
                P.R[P.Rpos[op][i]]=0;
                P.R[newpos]=1;
                P.Rpos[op][i]=newpos;

                if(P.B[newpos]==1)
                {
                    P.B[newpos]=0;
                    auto it = std::find(P.Bpos.begin(), P.Bpos.end(), newpos);
                    P.Bpos.erase(it);
                    reactionsAffected= {0,1,2,3,4,5,6};//B is removed so diffusionB,bindingB and unbindingB need to be updated
                }
            }
            break;
        }
        }
    }


    double rate(const Species &P)
    {
        //std::cout << rate_const*P.Rpos[op].size() << '\n';
        return rate_const*P.Rpos[op].size();
    }

protected:

private:
};

#endif // ELONGATION_H
