#ifndef BINDINGR_H
#define BINDINGR_H

#include <Reaction.h>


class bindingR : public Reaction
{
    public:
        size_t pos;
        size_t op;

        bindingR(double k,size_t pos,size_t op):pos(pos),op(op) {
        rate_const=k;}

        virtual ~bindingR() {}


        void do_reaction(Species &P, double rx_u, double rate){

            if(P.B[pos]==0 && P.R[pos]==0 ){
                P.R[pos]=1;
                P.Rpos[op].emplace_back(pos);
                reactionsAffected={op+7};//affects elongation in this operon
                //std::cout << "bindingR" << '\n';
                return;
            }
        }


        double rate(const Species &P){
            return rate_const;
        }

    protected:

    private:
};

#endif // BINDINGR_H
