#ifndef DIFFUSIONB_H
#define DIFFUSIONB_H

#include <Reaction.h>
#include <math.h>


class diffusionB : public Reaction
{
    public:
        diffusionB(double d) {
        rate_const=2*d;
        }

        virtual ~diffusionB() {}


        void do_reaction(Species &P, double rx_u, double rate){
            if(P.Bpos.size()==0) return;

            size_t i=rx_u/rate_const;//index in Bpos
            int dir=2*round((rx_u-i*rate_const)/rate_const)-1;//slide left (-1) or right (+1)
//            if(i>=P.Bpos.size()){
//                std::cout << rx_u << '\t' << rate << '\t' << rx_u/rate_const << '\t' << i << '\t' << P.Bpos.size();
//            }
            assert(i<P.Bpos.size());
            long int newpos=long(P.Bpos[i])+dir;

            //assert(newpos>-2 && newpos <long(L)+2);
            //std::cout << dir << '\n';


            if(P.in_region(newpos)==0){//fall off the edge
                    assert(P.Bpos[i]>=0);
                P.B[P.Bpos[i]]=0;
                P.Bpos.erase(P.Bpos.begin()+i);
                reactionsAffected={0,1,2,3,4,5,6};//affects diffusionB, bindingB and unbindingB
               return;
            }

            if(P.B[newpos]==0 && P.R[newpos]==0 ){
                P.B[P.Bpos[i]]=0;
                P.B[newpos]=1;
                P.Bpos[i]=newpos;
                //std::cout << "diffusing" << '\n';
                return;
            }
        }


        double rate(const Species &P){
            return rate_const*P.Bpos.size();//
        }


    protected:

    private:
};

#endif // DIFFUSIONB_H
