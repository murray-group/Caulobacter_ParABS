#ifndef BINDINGB_H
#define BINDINGB_H

#include <Reaction.h>


class bindingB : public Reaction
{
    public:
        size_t Btot;
        size_t pos;


        bindingB(double k,size_t Btot, size_t pos):Btot(Btot),pos(pos){
            rate_const=k;}

        virtual ~bindingB() {}




        void do_reaction(Species &P, double rx_u, double rate){

            if(P.B[pos]==0 && P.R[pos]==0 ){
                P.B[pos]=1;
                P.Bpos.emplace_back(pos);
                //std::cout << "binding" << '\n';
                reactionsAffected={0,1,2,3,4,5,6};//affects diffusion, bindingB and unbindingB
                return;
            }
        }


        double rate(const Species &P){
            return rate_const*(Btot-P.Bpos.size());
        }


    protected:

    private:
};

#endif // BINDINGB_H
