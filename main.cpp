#include <iostream>	// cout, etc.
#include <vector>
# include <fstream>      //output to file


#include "GillespieSimulation.h"
#include "diffusionB.h"
#include "unbindingB.h"
#include "bindingB.h"
#include "elongation.h"
#include "bindingR.h"

using namespace std ;


int main(int argc,char *argv[]){

double d, dB, kB, kR, ke;
size_t Btot;
        if(argc==7)
    {
        d = atof(argv[1]);
        dB = atof(argv[2]);
        kB = atof(argv[3]);
        kR = atof(argv[4]);
        ke = atof(argv[5]);
        Btot=atoi(argv[6]);
    }
    else
    {
        d=50.0;
        dB=0.0116;
        kB=92.4;
        kR=0.0002;//0.3/75, average is 0.33 initiations per sec per gene
        ke=40.0;//75 nt/s
        Btot=300;
        cout << "Using default parameters." << '\n';
    }

Species P;


ReactionsArray RX;
RX.resize(17);

RX[0].reset(new diffusionB(d));
RX[1].reset(new unbindingB(dB));
RX[2].reset(new bindingB(kB/30, Btot,403496-start));
RX[3].reset(new bindingB(kB/62, Btot,403490-start));
RX[4].reset(new bindingB(kB/133, Btot,403779-start));
RX[5].reset(new bindingB(kB/144, Btot,403322-start));
RX[6].reset(new bindingB(kB/170, Btot,403542-start));
RX[7].reset(new elongation(ke,403055-start,403479-start,-1,0));
RX[8].reset(new elongation(ke,403496-start,403656-start,-1,1));
RX[9].reset(new elongation(ke,403664-start,403835-start,1,2));
RX[10].reset(new elongation(ke,403843-start,403987-start,-1,3));
RX[11].reset(new elongation(ke,404013-start,404268-start,-1,4));
RX[12].reset(new bindingR(kR*51,403479-start,0));
RX[13].reset(new bindingR(kR*39,403656-start,1));
RX[14].reset(new bindingR(kR*31,403664-start,2));
RX[15].reset(new bindingR(kR*97,403987-start,3));
RX[16].reset(new bindingR(kR*113,404268-start,4));


for(auto pos : P.Bpos){
    std::cout << pos << '\t';
}

GillespieSimulation gs(RX,P);//P stored as reference

gs.advance_time(100.0);

ofstream out;
out.open("ParB.txt");
P.print(out);

double t=0.0;
double t_final=60*10.0;
double t_step=10.0;

while(t<t_final){
    gs.advance_time(t_step);
    P.print(out);
    t=t+t_step;
}





std::cout << P.Bpos.size() << '\t' << P.Rpos[0].size()+P.Rpos[1].size()+P.Rpos[2].size()+P.Rpos[3].size()+P.Rpos[4].size();




return 0;


}
